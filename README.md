# Samaritan

A demo like Samaritan from POI.

![Screenshot](https://i.imgur.com/xd5QHjz.jpg)


## Usage

You can edit `text.json` to show whatever you like. First numeric element in
array is the delay timeout in seconds after message is shown.
