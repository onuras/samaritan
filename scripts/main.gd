extends Node2D

onready var label = get_node("Label")
onready var _timer = Timer.new()

var done = false
var sequence = []
var sequence_index = 0;
var VECIZE = []
var VECIZE_DEFAULT = [
  [ 0.1, "dosya -patlak" ],
]

func _ready():
	randomize()
	add_child(_timer)
	_timer.set_wait_time(0.06)
	_timer.set_one_shot(false)
	_timer.connect("timeout", self, "_show_sequence")
	set_opacity(0)
	if not _load_vecize():
		VECIZE = VECIZE_DEFAULT
	_gen_sequence()


func _load_vecize():
	var file = File.new()
	if file.open("text.json", file.READ) != OK:
		return false
	var vecize_text = file.get_as_text()
	file.close()
	var dict = {}
	if dict.parse_json(vecize_text) != OK:
		return false
	if not dict.has("samaritan"):
		return false
	VECIZE = dict.samaritan
	return true

func _random_word(size):
	var chars_max = 26;
	var chars = "abcdefghijklmnopqrstuvwxyz";
	var word = '';
	for i in range(size):
		word += chars[randi() % 26];
	return word


func _show_sequence():
	if sequence_index < sequence.size():
		label.set_text(sequence[sequence_index])
		sequence_index += 1
	else:
		_timer.stop()

func _gen_sequence():
	for i in range(30):
		sequence.append('')
	
	for line_raw in VECIZE:
		var line_wait = line_raw[0]
		var line = line_raw[1]
		
		# first need to calculate width of line
		var line_width = 0
		for i in line.split("-"):
			line_width += i.length()
		
		
		# append 6 random text first
		for i in range(6):
			sequence.append(_random_word(line_width))
			
		var kelime = '';
		for line_part in line.split("-"):
			kelime += line_part.to_upper()
			# add more random words to bottom
			if kelime.length() < line_width:
				for i in range(10):
					var kelime_full = kelime + _random_word(line_width - kelime.length())
					sequence.append(kelime_full)
			else:
				for i in range(15 * line_wait):
					sequence.append(kelime)
		
		

func _on_LoadPlayer_finished():
	get_node("Cursor/AnimationPlayer").play("Cursor")
	_timer.start()